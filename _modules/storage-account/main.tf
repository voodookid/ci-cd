provider "azurerm" {}

resource "random_id" "byte8" {
  byte_length = 8
}

resource "azurerm_storage_account" "sa" {
  name                     = "${lower(random_id.byte8.hex)}"
  resource_group_name      = "${var.resource-group-name}"
  location                 = "${var.resource-group-location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}