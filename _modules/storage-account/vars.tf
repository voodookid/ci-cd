variable "resource-group-name" {
  type        = "string"
  description = "Name of the resource group you are creating the availability set int"
}
variable "resource-group-location" {
  type        = "string"
  description = "Location of the resource group you are creating the availability set in"
}