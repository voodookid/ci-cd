variable "location" {
  description = "Location that you will create the resource group in"
}
variable "name" {
  description = "Name of the resource group you will create"
}
