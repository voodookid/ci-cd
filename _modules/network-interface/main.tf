provider "azurerm" {}

resource "random_id" "byte4" {
  byte_length = 4
}

resource "azurerm_network_interface" "net-public-interface" {
  # If var.public-interface is set to true, count will equal 1 and create the resource, otherwise count equals 0
  count               = "${var.public-interface}"
  name                = "${var.interface-name}"
  resource_group_name = "${var.resource-group-name}"
  location            = "${var.resource-group-location}"

    ip_configuration {
      name                          = "${var.interface-name}-${random_id.byte4.hex}"
      private_ip_address_allocation = "${var.static-dynamic}"
      subnet_id                     = "${var.associated-subnet-id}"
      public_ip_address_id          = "${var.public-ip-assocation}"
    }
}

resource "azurerm_network_interface" "net-private-interface" {
  # Same as above except this time using simple math
  # If var.public-interface is true, 1 - 1 = 0 and nothing is created
  # Otherwise var.public-interface = 0, 1 - 0 = 1, and the resource is created
  count               = "${1 - var.public-interface}"
  name                = "${var.interface-name}"
  resource_group_name = "${var.resource-group-name}"
  location            = "${var.resource-group-location}"

    ip_configuration {
      name                          = "${var.interface-name}-${random_id.byte4.hex}"
      private_ip_address_allocation = "${var.static-dynamic}"
      subnet_id                     = "${var.associated-subnet-id}"
    }
}

