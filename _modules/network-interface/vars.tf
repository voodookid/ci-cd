variable "interface-name" {
  description = "Name associated with the interface you are creating"
  type        = "string"
}
variable "resource-group-name" {
  description = "Name of the resource group that you will create the interface in"
  type        = "string"
}
variable "resource-group-location" {
  description = "Location of the resource group that you will create the interface in"
  type        = "string"
}
variable "static-dynamic" {
  description = "Acceptable values are static or dynamic, used for IP configuration of the interface"
  type        = "string"
}
variable "associated-subnet-id" {
  description = "Subnet ID that you want to associate the interface with"
  type        = "string"
}
variable "public-interface" {
  description = "Accepted values are true/false, no quotations, true will create a public interface, false only a private"
  type        = "string"
}
variable "public-ip-assocation" {
  description = "Only needed if you are creating a public facing interface, value is the public ip ID, otherwise leave blank"
  type        = "string"
}

