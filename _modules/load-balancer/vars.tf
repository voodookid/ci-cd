variable "resource-group-name" {
  type        = "string"
  description = "Name of the resource group you are creating the availability set int"
}
variable "resource-group-location" {
  type        = "string"
  description = "Location of the resource group you are creating the availability set in"
}
variable "lb-name" {
  type        = "string"
  description = "Name you are giving to the load balancer you are creating"
}
variable "front-end-name" {
  type        = "string"
  description = "Name you are giving to the frontend IP configuration"
}
variable "ip-allocation" {
  type        = "string"
  description = "Variable must be defined as either dynamic/static"
}
variable "subnet-association" {
  type        = "string"
  description = "Subnet ID you wish to place this load balancer in"
}
variable "private-ip-address" {
  type        = "string"
  description = "IP address you want to statically assign to the load balancer, can be left blank if ip-allocation is dynamic"
}
variable "public-ip-address" {
  type        = "string"
  description = "IP address should be already created in Azure, assigns it to the load balancer"
}

