provider "azurerm" {}

resource "azurerm_lb" "azure-lb" {
  name                = "${var.lb-name}"
  location            = "${var.resource-group-location}"
  resource_group_name = "${var.resource-group-name}"
  sku                 = "Standard"

  frontend_ip_configuration {
    name                          = "${var.front-end-name}"
    private_ip_address_allocation = "${var.ip-allocation}"
    private_ip_address            = "${var.private-ip-address}"
    public_ip_address_id          = "${var.public-ip-address}"
    subnet_id                     = "${var.subnet-association}"
  }
}
