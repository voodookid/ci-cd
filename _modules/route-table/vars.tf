variable "resource-group-name" {
  description = "Name of the resource group that you will create the interface in"
  type        = "string"
}
variable "resource-group-location" {
  description = "Location of the resource group that you will create the interface in"
  type        = "string"
}
variable "rt-name" {
  description = "Name of the route table you are going to create"
}
