provider "azurerm" {}

resource "azurerm_route_table" "rt" {
  name                = "${var.rt-name}"
  location            = "${var.resource-group-location}"
  resource_group_name = "${var.resource-group-name}"
}