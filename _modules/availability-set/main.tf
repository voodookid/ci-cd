provider "azurerm" {}

resource "azurerm_availability_set" "panw-fw-availabilityset" {
  name                = "${var.as-name}"
  resource_group_name = "${var.resource-group-name}"
  location            = "${var.resource-group-location}"
}




