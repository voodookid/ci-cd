provider "azurerm" {}

resource "random_id" "byte2" {
  byte_length = 2
}

resource "azurerm_virtual_machine" "sec-op-fw" {
  name                         = "pan-${random_id.byte2}"
  location                     = "${var.resource-group-location}"
  availability_set_id          = "${var.availability-set}"
  vm_size                      = "Standard_D3"
  network_interface_ids        = ["${var.network-interfaces-attached}"]
  primary_network_interface_id = "${var.network-interface-primary}"
  resource_group_name          = "${var.resource-group-name}"
  storage_image_reference {
    publisher = "paloaltonetworks"
    offer     = "vmseries1"
    sku       = "bundle1"
    version   = "latest"
  }
    storage_os_disk {
    name          = "osdisk1"
    vhd_uri       = "${var.storage-account-id}-${var.storage-container-name}-${azurerm_virtual_machine.sec-op-fw.name}.vhd"
    caching       = "ReadWrite"
    create_option = "FromImage"
  }
  os_profile {
    computer_name  = "${var.firewall-name}"
    admin_username = "${var.firewall-username}"
    admin_password = "${var.firewall-password}"

  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  plan {
    name      = "bundle1"
    product   = "vmseries1"
    publisher = "paloaltonetworks"
  }
}
