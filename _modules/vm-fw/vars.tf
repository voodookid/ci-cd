variable "resource-group-name" {
  type        = "string"
  description = "Name of the resource group you are creating the availability set int"
}
variable "resource-group-location" {
  type        = "string"
  description = "Location of the resource group you are creating the availability set in"
}
variable "network-interfaces-attached" {
  description = "List of the network interface already created you wish to attach"
  type        = "list"
}
variable "network-interface-primary" {
  description = "Should be set to the management interface created for the FW in most instances"
}
variable "availability-set" {
  description = "The availability set ID that you wish to place the firewall into"
}
variable "storage-account-id" {
  description = "Storage account ID where you wish to store the firewall disk image"
}
variable "storage-container-name" {
  description = "Storage container name where you wish to store the firewall disk image"
}
variable "firewall-name" {
  description = "Name to be assigned in PANOS to the firewall you are deploying"
}
variable "firewall-username" {
  description = "Username of the initial admin account on the firewall"
}
variable "firewall-password" {
  description = "Password of the intial admin account on the firewall"
}
