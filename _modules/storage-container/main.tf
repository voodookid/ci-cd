provider "azurerm" {}

resource "random_id" "byte4" {
  byte_length = 4
}

resource "azurerm_storage_container" "fw-sc" {
  name                  = "fw-${lower(random_id.byte4.hex)}"
  resource_group_name   = "${var.resource-group-name}"
  storage_account_name  = "${var.storage-account-name}"
  container_access_type = "private"
}