variable "resource-group-name" {
  type        = "string"
  description = "Name of the resource group you are creating the availability set int"
}
variable "storage-account-name" {
  description = "Name of the storage account you wish to place the container into"
}