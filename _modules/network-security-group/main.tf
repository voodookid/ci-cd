provider "azurerm" {}

resource "azurerm_network_security_group" "nsg" {
  location            = "${var.resource-group-location}"
  name                = "${var.security-group-name}"
  resource_group_name = "${var.resource-group-name}"
}