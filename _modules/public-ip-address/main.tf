provider "azurerm" {}

resource "random_id" "byte2" {
  byte_length = 2
}

resource "azurerm_public_ip" "public-network-ip" {
  location                      = "${var.resource-group-location}"
  name                          = "${random_id.byte2.hex}-public-IP"
  public_ip_address_allocation  = "${var.static-dynamic}"
  resource_group_name           = "${var.resource-group-name}"
}
