variable "static-dynamic" {
  description = "Accepted values are static and dynamic, based on the allocation that you wish to use"
  type        = "string"
}
variable "resource-group-name" {
  description = "Name of the resource group that you will create the interface in"
  type        = "string"
}
variable "resource-group-location" {
  description = "Location of the resource group that you will create the interface in"
  type        = "string"
}