variable "resource-group-name" {
  type        = "string"
  description = "Name of the resource group you are creating the availability set int"
}
variable "allow-block" {
  type        = "string"
  description = "Determines if you want to allow or deny the traffic, accepted values are allow/deny "
}
variable "direction" {
  type        = "string"
  description = "Determines direction of the traffic, accepted values are inbound/outbound"
}
variable "security-rule-name" {
  type        = "string"
  description = "Name of the security rule you are creating"
}
variable "priority" {
  type        = "string"
  description = "Determines the location the rule will be placed in the existing rule base, allowed values 0-500"
}
variable "protocol" {
  type        = "string"
  description = "Determines the network protocol of the traffic you are writing the rule for"
}
variable "network-security-group" {
  type        = "string"
  description = "Name of the NSG that you want to apply the rule to, should be an existing resource"
}
variable "source-address-prefix" {
  type        = "string"
  default     = "*"
  description = "Source IP address(es), CIDR notation or * for all"
}
variable "destination-address-prefix" {
  type        = "string"
  default     = "*"
  description = "Destination IP address(es), CIDR notation or * for all"
}
variable "source-port-range" {
  type        = "string"
  default     = "*"
  description = "Source port/range, example 22, 1024-55535, or * for all"
}
variable "destination-port-range" {
  type        = "string"
  default     = "*"
  description = "Destination port/range, example 22, 1024-55535, or * for all"
}

