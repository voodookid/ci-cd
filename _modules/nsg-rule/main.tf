provider "azurerm" {}

# TODO, probably makes more sense to format last four variables into arrays vs string variables

resource "azurerm_network_security_rule" "nsg-rule" {
  access                      = "${var.allow-block}"
  direction                   = "${var.direction}"
  name                        = "${var.security-rule-name}"
  network_security_group_name = "${var.network-security-group}"
  priority                    = "${var.priority}"
  protocol                    = "${var.protocol}"
  resource_group_name         = "${var.resource-group-name}"
  source_address_prefix       = "${var.source-address-prefix}"
  destination_address_prefix  = "${var.destination-address-prefix}"
  source_port_range           = "${var.source-port-range}"
  destination_port_range      = "${var.destination-port-range}"
}
