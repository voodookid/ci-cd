data "terraform_remote_state" "network-core" {
  backend = "local"
  config {
    path = "../1-network-core/terraform.tfstate"
  }
}




# Creates public IP addresses for use on the firewall management interfaces
module "reg1-bnet-fw1-mgmt-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
}
module "reg1-gnet-fw1-mgmt-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
}
module "reg2-bnet-fw1-mgmt-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
}
module "reg2-gnet-fw1-mgmt-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
}
module "reg1-bnet-fw2-mgmt-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
}
module "reg1-gnet-fw2-mgmt-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
}
module "reg2-bnet-fw2-mgmt-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
}
module "reg2-gnet-fw2-mgmt-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
}

output "reg1-bnet-fw1-mgmt-pip" {
  value = "${module.reg1-bnet-fw1-mgmt-pip.ip-address}"
}
output "reg1-bnet-fw2-mgmt-pip" {
  value = "${module.reg1-bnet-fw2-mgmt-pip.ip-address}"
}
output "reg1-gnet-fw1-mgmt-pip" {
  value = "${module.reg1-gnet-fw1-mgmt-pip.ip-address}"
}
output "reg1-gnet-fw2-mgmt-pip" {
  value = "${module.reg1-gnet-fw2-mgmt-pip.ip-address}"
}
output "reg2-bnet-fw1-mgmt-pip" {
  value = "${module.reg2-bnet-fw1-mgmt-pip.ip-address}"
}
output "reg2-bnet-fw2-mgmt-pip" {
  value = "${module.reg2-bnet-fw2-mgmt-pip.ip-address}"
}
output "reg2-gnet-fw1-mgmt-pip" {
  value = "${module.reg2-gnet-fw1-mgmt-pip.ip-address}"
}
output "reg2-gnet-fw2-mgmt-pip" {
  value = "${module.reg2-gnet-fw2-mgmt-pip.ip-address}"
}




# Creates public IP addresses for use on the firewall external interfaces
module "reg1-bnet-fw1-ext-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
}
module "reg1-gnet-fw1-ext-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
}
module "reg2-bnet-fw1-ext-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
}
module "reg2-gnet-fw1-ext-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
}
module "reg1-bnet-fw2-ext-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
}
module "reg1-gnet-fw2-ext-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
}
module "reg2-bnet-fw2-ext-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
}
module "reg2-gnet-fw2-ext-pip" {
  source                    = "../../../_modules/public-ip-address"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "static"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
}



//# Creates internal load balancers for the vnets existing in both regions
module "reg1-bnet-internal-lb" {
  source                    = "../../../_modules/load-balancer"
  "lb-name"                 = "reg1-bnet-internal-lb"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "ip-allocation"           = "dynamic"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
  "front-end-name"          = "reg1-bnet-internal-lb-fe"
  "private-ip-address"      = ""
  "subnet-association"      = "${data.terraform_remote_state.network-core.bvnet-reg1-vnet-subnets[4]}"
}
module "reg1-gnet-internal-lb" {
  source                    = "../../../_modules/load-balancer"
  "lb-name"                 = "reg1-gnet-internal-lb"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "ip-allocation"           = "dynamic"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
  "front-end-name"          = "reg1-gnet-internal-lb-fe"
  "private-ip-address"      = ""
  "subnet-association"      = "${data.terraform_remote_state.network-core.gvnet-reg1-vnet-subnets[4]}"
}
module "reg2-bnet-internal-lb" {
  source                    = "../../../_modules/load-balancer"
  "lb-name"                 = "reg2-bnet-internal-lb"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "ip-allocation"           = "dynamic"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
  "front-end-name"          = "reg2-bnet-internal-lb-fe"
  "private-ip-address"      = ""
  "subnet-association"      = "${data.terraform_remote_state.network-core.bvnet-reg2-vnet-subnets[4]}"
}
module "reg2-gnet-internal-lb" {
  source                    = "../../../_modules/load-balancer"
  "lb-name"                 = "reg2-gnet-internal-lb"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "ip-allocation"           = "dynamic"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
  "front-end-name"          = "reg2-gnet-internal-lb-fe"
  "private-ip-address"      = ""
  "subnet-association"      = "${data.terraform_remote_state.network-core.gvnet-reg2-vnet-subnets[4]}"
}




//# Creates availability sets in both regions, used by firewalls once deployed
module "reg1-bnet-fw-availability-set" {
  source                    = "../../../_modules/availability-set"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "as-name"                 = "reg1-bnet-fw-as"
}
module "reg1-gnet-fw-availability-set" {
  source                    = "../../../_modules/availability-set"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "as-name"                 = "reg1-gnet-fw-as"
}
module "reg2-bnet-fw-availability-set" {
  source                    = "../../../_modules/availability-set"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "as-name"                 = "reg2-bnet-fw-as"
}
module "reg2-gnet-fw-availability-set" {
  source                    = "../../../_modules/availability-set"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "as-name"                 = "reg2-gnet-fw-as"
}




//# Creates network interfaces for all eight firewalls to be deployed
module "reg1-bnet-fw1" {
  source                    = "../../../_modules/network-interface"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
  "interface-name"          = "bnet-fw1-mgmt-interface"
  "associated-subnet-id"    = "${data.terraform_remote_state.network-core.bvnet-reg1-vnet-subnets[0]}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "dynamic"
  "public-interface"        = true
  "public-ip-assocation"    = "${module.reg1-bnet-fw1-mgmt-pip.ip-address}"
}
module "reg1-bnet-fw2" {
  source                    = "../../../_modules/network-interface"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
  "interface-name"          = "bnet-fw2-mgmt-interface"
  "associated-subnet-id"    = "${data.terraform_remote_state.network-core.bvnet-reg1-vnet-subnets[0]}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "dynamic"
  "public-interface"        = true
  "public-ip-assocation"    = "${module.reg1-bnet-fw2-mgmt-pip.ip-address}"
}
module "reg1-gnet-fw1" {
  source                    = "../../../_modules/network-interface"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
  "interface-name"          = "gnet-fw1-mgmt-interface"
  "associated-subnet-id"    = "${data.terraform_remote_state.network-core.gvnet-reg1-vnet-subnets[0]}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "dynamic"
  "public-interface"        = true
  "public-ip-assocation"    = "${module.reg1-gnet-fw1-mgmt-pip.ip-address}"
}
module "reg1-gnet-fw2" {
  source                    = "../../../_modules/network-interface"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg1-name}"
  "interface-name"          = "gnet-fw2-mgmt-interface"
  "associated-subnet-id"    = "${data.terraform_remote_state.network-core.gvnet-reg1-vnet-subnets[0]}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg1-location}"
  "static-dynamic"          = "dynamic"
  "public-interface"        = true
  "public-ip-assocation"    = "${module.reg1-gnet-fw2-mgmt-pip.ip-address}"
}


module "reg2-bnet-fw1" {
  source                    = "../../../_modules/network-interface"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
  "interface-name"          = "bnet-fw1-mgmt-interface"
  "associated-subnet-id"    = "${data.terraform_remote_state.network-core.bvnet-reg2-vnet-subnets[0]}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "dynamic"
  "public-interface"        = true
  "public-ip-assocation"    = "${module.reg2-bnet-fw1-mgmt-pip.ip-address}"
}
module "reg2-bnet-fw2" {
  source                    = "../../../_modules/network-interface"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
  "interface-name"          = "bnet-fw2-mgmt-interface"
  "associated-subnet-id"    = "${data.terraform_remote_state.network-core.bvnet-reg2-vnet-subnets[0]}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "dynamic"
  "public-interface"        = true
  "public-ip-assocation"    = "${module.reg2-bnet-fw2-mgmt-pip.ip-address}"
}
module "reg2-gnet-fw1" {
  source                    = "../../../_modules/network-interface"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
  "interface-name"          = "gnet-fw1-mgmt-interface"
  "associated-subnet-id"    = "${data.terraform_remote_state.network-core.gvnet-reg2-vnet-subnets[0]}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "dynamic"
  "public-interface"        = true
  "public-ip-assocation"    = "${module.reg2-gnet-fw1-mgmt-pip.ip-address}"
}
module "reg2-gnet-fw2" {
  source                    = "../../../_modules/network-interface"
  "resource-group-name"     = "${data.terraform_remote_state.network-core.rg2-name}"
  "interface-name"          = "gnet-fw2-mgmt-interface"
  "associated-subnet-id"    = "${data.terraform_remote_state.network-core.gvnet-reg2-vnet-subnets[0]}"
  "resource-group-location" = "${data.terraform_remote_state.network-core.rg2-location}"
  "static-dynamic"          = "dynamic"
  "public-interface"        = true
  "public-ip-assocation"    = "${module.reg2-gnet-fw2-mgmt-pip.ip-address}"
}

