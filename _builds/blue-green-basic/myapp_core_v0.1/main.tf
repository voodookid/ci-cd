
# Creates resource groups - outputs name and location from both to be used in other modules
# -----------------------------------------------------------------------------------------
module "app-region1" {
    source              = "../../../_modules/resource-groups"
    location            = "centralus"
    name                = "app1-region1"
}
module "app-region2" {
    source              = "../../../_modules/resource-groups"
    location            = "westus"
    name                = "app1-region2"
}
output "rg1-name" {
  value                 = "${module.app-region1.name}"
}
output "rg2-name" {
  value                 = "${module.app-region2.name}"
}
output "rg1-location" {
  value                 = "${module.app-region1.location}"
}
output "rg2-location" {
  value                 = "${module.app-region2.location}"
}
# -----------------------------------------------------------------------------------------




# Create storage accounts in each region - output account names for use later
#-----------------------------------------------------------------------------------------
module "sa-region1" {
    source                    = "../../../_modules/storage-account"
    resource-group-location = "${module.app-region1.location}"
    resource-group-name     = "${module.app-region1.name}"
}
module "sa-region2" {
    source                    = "../../../_modules/storage-account"
    resource-group-location   = "${module.app-region2.location}"
    resource-group-name      = "${module.app-region2.name}"
}
output "sa-region1" {
  value                       = "${module.sa-region1.name}"
}
output "sa-region2" {
  value                       = "${module.sa-region2.name}"
}
#-----------------------------------------------------------------------------------------




# Create Blue network in region 1 - route table for app-net - output network/route info for use in other builds
#-----------------------------------------------------------------------------------------
module "bvnet-reg1" {
    source              = "Azure/vnet/azurerm"
    resource_group_name = "${module.app-region1.name}"
    location            = "${module.app-region1.location}"
    vnet_name           = "app1-bnet-reg1"
    address_space       = "172.16.0.0/24"
    subnet_prefixes     = ["172.16.0.0/28", "172.16.0.16/28", "172.16.0.32/28", "172.16.0.48/28", "172.16.0.64/28", "172.16.0.80/28", "172.16.0.96/28"]
    subnet_names        = ["fw-mgmt-net", "fw-untrust-net", "fw-trust-net", "ext-lb-net", "int-inbound-lb-net", "int-outbound-lb-net", "app-net"]

    tags                = {
                            environment = "dev"
                            cicd        = "blue"
                            region      = "1"
                          }
}
module "bnet-app-route" {
    source                  = "../../../_modules/route-table"
    resource-group-name     = "${module.app-region1.name}"
    resource-group-location = "${module.app-region1.location}"
    rt-name                 = "app-net-route-table"
}
output "bvnet-reg1-vnet-subnets" {
  value = "${module.bvnet-reg1.vnet_subnets}"
}
output "bnet-reg1-app-rt" {
  value = "${module.bnet-app-route.route-table-name}"
}
#-----------------------------------------------------------------------------------------




# Create green network in region 1 - output network info for use in other builds
#-----------------------------------------------------------------------------------------
module "gvnet-reg1" {
    source              = "Azure/vnet/azurerm"
    resource_group_name = "${module.app-region1.name}"
    location            = "${module.app-region1.location}"
    vnet_name           = "app1-gnet-reg1"
    address_space       = "172.18.0.0/24"
    subnet_prefixes     = ["172.18.0.0/28", "172.18.0.16/28", "172.18.0.32/28", "172.18.0.48/28", "172.18.0.64/28", "172.18.0.80/28", "172.18.0.96/28"]
    subnet_names        = ["fw-mgmt-net", "fw-untrust-net", "fw-trust-net", "ext-lb-net", "int-inbound-lb-net", "int-outbound-lb-net", "app-net"]

    tags                = {
                            environment = "dev"
                            cicd        = "green"
                            region      = "1"
                          }
}
output "gvnet-reg1-vnet-subnets" {
  value = "${module.gvnet-reg1.vnet_subnets}"
}
#-----------------------------------------------------------------------------------------




# Create blue network in region 2 - output network info for use in other builds
#-----------------------------------------------------------------------------------------
module "bvnet-reg2" {
    source              = "Azure/vnet/azurerm"
    resource_group_name = "${module.app-region2.name}"
    location            = "${module.app-region2.location}"
    vnet_name           = "app1-bnet-reg2"
    address_space       = "172.17.0.0/24"
    subnet_prefixes     = ["172.17.0.0/28", "172.17.0.16/28", "172.17.0.32/28", "172.17.0.48/28", "172.17.0.64/28", "172.17.0.80/28", "172.17.0.96/28"]
    subnet_names        = ["fw-mgmt-net", "fw-untrust-net", "fw-trust-net", "ext-lb-net", "int-inbound-lb-net", "int-outbound-lb-net", "app-net"]

    tags                = {
                            environment = "dev"
                            cicd        = "blue"
                            region      = "2"
                          }
}
output "bvnet-reg2-vnet-subnets" {
  value = "${module.bvnet-reg2.vnet_subnets}"
}
#-----------------------------------------------------------------------------------------




# Create green network in region 2 - output network info for use in other builds
#-----------------------------------------------------------------------------------------
module "gvnet-reg2" {
    source              = "Azure/vnet/azurerm"
    resource_group_name = "${module.app-region2.name}"
    location            = "${module.app-region2.location}"
    vnet_name           = "app1-gnet-reg2"
    address_space       = "172.19.0.0/24"
    subnet_prefixes     = ["172.19.0.0/28", "172.19.0.16/28", "172.19.0.32/28", "172.19.0.48/28", "172.19.0.64/28", "172.19.0.80/28", "172.19.0.96/28"]
    subnet_names        = ["fw-mgmt-net", "fw-untrust-net", "fw-trust-net", "ext-lb-net", "int-inbound-lb-net", "int-outbound-lb-net", "app-net"]

    tags                = {
                            environment = "dev"
                            cicd        = "green"
                            region      = "2"
                          }
}
output "gvnet-reg2-vnet-subnets" {
  value = "${module.gvnet-reg2.vnet_subnets}"
}
#-----------------------------------------------------------------------------------------




# Create NSGs to be used for very basic protection
#-----------------------------------------------------------------------------------------
module "fw-mgmt-nsg-region1" {
    source                  = "../../../_modules/network-security-group"
    security-group-name     = "fw-mgmt-nsg"
    resource-group-name     = "${module.app-region1.name}"
    resource-group-location = "${module.app-region1.location}"
}
module "fw-mgmt-nsg-region2" {
    source                  = "../../../_modules/network-security-group"
    security-group-name     = "fw-mgmt-nsg"
    resource-group-name     = "${module.app-region2.name}"
    resource-group-location = "${module.app-region2.location}"
}
module "allow-all-nsg-region1" {
    source                  = "../../../_modules/network-security-group"
    security-group-name     = "allow-all-fw-controlled"
    resource-group-name     = "${module.app-region1.name}"
    resource-group-location = "${module.app-region1.location}"
}
module "allow-all-nsg-region2" {
    source                  = "../../../_modules/network-security-group"
    security-group-name     = "allow-all-fw-controlled"
    resource-group-name     = "${module.app-region2.name}"
    resource-group-location = "${module.app-region2.location}"
}
module "fwmgmt-rule1-region1" {
    source                      = "../../../_modules/nsg-rule"
    protocol                    = "tcp"
    allow-block                 = "allow"
    direction                   = "inbound"
    priority                    = "110"
    security-rule-name          = "allow-ssh"
    source-address-prefix       = "66.41.96.28/32"
    destination-address-prefix  = "*"
    source-port-range           = "*"
    destination-port-range      = "22"
    resource-group-name         = "${module.app-region1.name}"
    network-security-group      = "${module.fw-mgmt-nsg-region1.security-group-name}"
}
module "fwmgmt-rule2-region1" {
    source                      = "../../../_modules/nsg-rule"
    protocol                    = "tcp"
    allow-block                 = "allow"
    direction                   = "inbound"
    priority                    = "120"
    security-rule-name          = "allow-https"
    source-address-prefix       = "66.41.96.28/32"
    destination-address-prefix  = "*"
    source-port-range           = "*"
    destination-port-range      = "443"
    resource-group-name         = "${module.app-region1.name}"
    network-security-group      = "${module.fw-mgmt-nsg-region1.security-group-name}"
}
module "fwmgmt-rule1-region2" {
    source                      = "../../../_modules/nsg-rule"
    protocol                    = "tcp"
    allow-block                 = "allow"
    direction                   = "inbound"
    priority                    = "110"
    security-rule-name          = "allow-ssh"
    source-address-prefix       = "66.41.96.28/32"
    destination-address-prefix  = "*"
    source-port-range           = "*"
    destination-port-range      = "22"
    resource-group-name         = "${module.app-region2.name}"
    network-security-group      = "${module.fw-mgmt-nsg-region2.security-group-name}"
}
module "fwmgmt-rule2-region2" {
    source                      = "../../../_modules/nsg-rule"
    protocol                    = "tcp"
    allow-block                 = "allow"
    direction                   = "inbound"
    priority                    = "120"
    security-rule-name          = "allow-https"
    source-address-prefix       = "66.41.96.28/32"
    destination-address-prefix  = "*"
    source-port-range           = "*"
    destination-port-range      = "443"
    resource-group-name         = "${module.app-region2.name}"
    network-security-group      = "${module.fw-mgmt-nsg-region2.security-group-name}"
}
module "allow-all-rule1-region1" {
    source                      = "../../../_modules/nsg-rule"
    protocol                    = "tcp"
    allow-block                 = "allow"
    direction                   = "inbound"
    priority                    = "110"
    security-rule-name          = "allow-all-tcp"
    source-address-prefix       = "*"
    destination-address-prefix  = "*"
    source-port-range           = "*"
    destination-port-range      = "*"
    resource-group-name         = "${module.app-region1.name}"
    network-security-group      = "${module.allow-all-nsg-region1.security-group-name}"
}
module "allow-all-rule2-region1" {
    source                      = "../../../_modules/nsg-rule"
    protocol                    = "udp"
    allow-block                 = "allow"
    direction                   = "inbound"
    priority                    = "120"
    security-rule-name          = "allow-all-udp"
    source-address-prefix       = "*"
    destination-address-prefix  = "*"
    source-port-range           = "*"
    destination-port-range      = "*"
    resource-group-name         = "${module.app-region1.name}"
    network-security-group      = "${module.allow-all-nsg-region1.security-group-name}"
}
module "allow-all-rule1-region2" {
    source                      = "../../../_modules/nsg-rule"
    protocol                    = "tcp"
    allow-block                 = "allow"
    direction                   = "inbound"
    priority                    = "110"
    security-rule-name          = "allow-all-tcp"
    source-address-prefix       = "*"
    destination-address-prefix  = "*"
    source-port-range           = "*"
    destination-port-range      = "*"
    resource-group-name         = "${module.app-region2.name}"
    network-security-group      = "${module.allow-all-nsg-region2.security-group-name}"
}
module "allow-all-rule2-region2" {
    source                      = "../../../_modules/nsg-rule"
    protocol                    = "udp"
    allow-block                 = "allow"
    direction                   = "inbound"
    priority                    = "120"
    security-rule-name          = "allow-all-udp"
    source-address-prefix       = "*"
    destination-address-prefix  = "*"
    source-port-range           = "*"
    destination-port-range      = "*"
    resource-group-name         = "${module.app-region2.name}"
    network-security-group      = "${module.allow-all-nsg-region2.security-group-name}"
}
output "fw-mgmt-nsg-region1" {
  value = "${module.fw-mgmt-nsg-region1.security-group-name}"
}
output "fw-mgmt-nsg-region2" {
  value = "${module.fw-mgmt-nsg-region2.security-group-name}"
}
output "allow-all-nsg-region1" {
  value = "${module.allow-all-nsg-region1.security-group-name}"
}
output "allow-all-nsg-region2" {
  value = "${module.allow-all-nsg-region2.security-group-name}"
}
#-----------------------------------------------------------------------------------------



# Creates availability sets that will house webservers and firewalls once deployed
#-----------------------------------------------------------------------------------------
module "fw-AS-blue-region1" {
    source                      = "../../../_modules/availability-set"
    "resource-group-location"   = "${module.app-region1.location}"
    "resource-group-name"       = "${module.app-region1.name}"
    "as-name"                   = "fw-AS-blue-region1"
}
module "fw-AS-blue-region2" {
    source                      = "../../../_modules/availability-set"
    "resource-group-location"   = "${module.app-region2.location}"
    "resource-group-name"       = "${module.app-region2.name}"
    "as-name"                   = "fw-AS-blue-region2"
}
module "fw-AS-green-region1" {
    source                      = "../../../_modules/availability-set"
    "resource-group-location"   = "${module.app-region1.location}"
    "resource-group-name"       = "${module.app-region1.name}"
    "as-name"                   = "fw-AS-green-region1"
}
module "fw-AS-green-region2" {
    source                      = "../../../_modules/availability-set"
    "resource-group-location"   = "${module.app-region2.location}"
    "resource-group-name"       = "${module.app-region2.name}"
    "as-name"                   = "fw-AS-green-region2"
}
module "web-AS-blue-region1" {
    source                      = "../../../_modules/availability-set"
    "resource-group-location"   = "${module.app-region1.location}"
    "resource-group-name"       = "${module.app-region1.name}"
    "as-name"                   = "web-AS-blue-region1"
}
module "web-AS-blue-region2" {
    source                      = "../../../_modules/availability-set"
    "resource-group-location"   = "${module.app-region2.location}"
    "resource-group-name"       = "${module.app-region2.name}"
    "as-name"                   = "web-AS-blue-region2"
}
module "web-AS-green-region1" {
    source                      = "../../../_modules/availability-set"
    "resource-group-location"   = "${module.app-region1.location}"
    "resource-group-name"       = "${module.app-region1.name}"
    "as-name"                   = "web-AS-green-region1"
}
module "web-AS-green-region2" {
    source                      = "../../../_modules/availability-set"
    "resource-group-location"   = "${module.app-region2.location}"
    "resource-group-name"       = "${module.app-region2.name}"
    "as-name"                   = "web-AS-green-region2"
}
#-----------------------------------------------------------------------------------------



# Creates public IP space for external load balancers
#-----------------------------------------------------------------------------------------
module "pip-extlb-reg1-blue" {
    source                      = "../../../_modules/public-ip-address"
    "resource-group-location"   = "${module.app-region1.location}"
    "static-dynamic"            = "static"
    "resource-group-name"       = "${module.app-region1.name}"
}
module "pip-extlb-reg1-green" {
    source                      = "../../../_modules/public-ip-address"
    "resource-group-location"   = "${module.app-region1.location}"
    "static-dynamic"            = "static"
    "resource-group-name"       = "${module.app-region1.name}"
}
module "pip-extlb-reg2-blue" {
    source                      = "../../../_modules/public-ip-address"
    "resource-group-location"   = "${module.app-region2.location}"
    "static-dynamic"            = "static"
    "resource-group-name"       = "${module.app-region2.name}"
}
module "pip-extlb-reg2-green" {
    source                      = "../../../_modules/public-ip-address"
    "resource-group-location"   = "${module.app-region2.location}"
    "static-dynamic"            = "static"
    "resource-group-name"       = "${module.app-region2.name}"
}
output "pip-extlb-reg1-blue" {
  value = "${module.pip-extlb-reg1-blue.ip-address}"
}
output "pip-extlb-reg1-green" {
  value = "${module.pip-extlb-reg1-green.ip-address}"
}
output "pip-extlb-reg2-blue" {
  value = "${module.pip-extlb-reg2-blue.ip-address}"
}
output "pip-extlb-reg2-green" {
  value = "${module.pip-extlb-reg2-green.ip-address}"
}
#-----------------------------------------------------------------------------------------




# Creates load balancers, load balancing rules, health probes, both for internal and external use
#-----------------------------------------------------------------------------------------
module "external-lb-blue-region1" {
    source                      = "../../../_modules/load-balancer"
    "front-end-name"            = "region1-public-ipconfig"
    "lb-name"                   = "region1-public-lb"
    "resource-group-name"       = "${module.app-region1.name}"
    "subnet-association"        = "${module.bvnet-reg1.vnet_subnets[3]}"
    "resource-group-location"   = "${module.app-region1.location}"

  # Fill in public IP ID already created if using externally, otherwise leave blank
    "public-ip-address"         = "${module.pip-extlb-reg1-blue.ip-address}"

  # Fill in ip-allocation and/or private-ip-address below if being used internally, otherwise leave blank
  # Fill in ip-allocation as either dynamic/static
  # If ip-allocation = dynamic, leave private-ip-address blank
  # If ip-allocation = static, enter private IP in /32 CIDR notation
    "ip-allocation"             = "dynamic"
    "private-ip-address"        = ""
}
